set -o errexit -o nounset -o noglob -o pipefail
command -v shopt > /dev/null && shopt -s failglob
(
    . /etc/os-release
    echo \
        $PRETTY_NAME \
        ${VERSION_ID:+$VERSION_ID}
)

oh () { true ; }


# export TERM="xterm-color"...
